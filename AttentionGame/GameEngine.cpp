#include "GameEngine.h"

GameEngine::GameEngine(QObject *parent) : QObject(parent)
{
    // create state machine engine
    engine = new QStateMachine(this);

    // create state timer
    timer = new QTimer(this);
    timer->setSingleShot(true);

    // add top-level states
    state_engineRunning = new QState(engine);
    state_enginePaused = new QState(engine);

    // add paused states
    state_engineLastActive = new QHistoryState(state_engineRunning);

    // set history transitions
    state_engineRunning->addTransition(this, SIGNAL(requestPause()), state_enginePaused);
    state_enginePaused->addTransition(this, SIGNAL(requestResume()), state_engineRunning);

    // set initial order
    engine->setInitialState(state_engineRunning);

    // connect states to method prototypes
    connect(state_engineRunning, SIGNAL(entered()), this, SLOT(onStateEngineRuning()));
    connect(state_enginePaused, SIGNAL(entered()), this, SLOT(onStateEnginePaused()));


}


GameEngine::~GameEngine()
{

}


void GameEngine::parseSettings(QString fileName_config)
{

    qDebug() << fileName_config ;

    // add QSettings
    QSettings *settings = new QSettings(fileName_config, QSettings::IniFormat);

    QStringList cueList = settings->childGroups();
    foreach (const QString cueCurrent, cueList)
    {


        settings->beginGroup(cueCurrent);
        QStringList cueKeysList = settings->childKeys();

        flow[cueCurrent] = new Cues(cueCurrent, state_engineRunning);

        foreach(const QString cueKey, cueKeysList)
        {
            QVariant cueValue = settings->value(cueKey);
            //flow[cueCurrent]->parseKeyValue(cueKey, cueValue);

            qDebug() << cueCurrent << cueKey << cueValue;
        }

        settings->endGroup();

    }


    /*
    QStringList childGroups=settings->childGroups();
    int state_id = 0;

    foreach (const QString &childGroups, childGroups)
    {
        settings->beginGroup(childGroups);
        const QStringList childKeys = settings->childKeys();

        flow[state_id] = new Cue(this);


        foreach (const QString &childKeys, childKeys)
        {
            QVariant value = settings->value(childKeys);
            qDebug() << childGroups << childKeys << value;

            if(QString::compare("transition", childKeys, Qt::CaseInsensitive) == 0)
            {
                flow[state_id]->id = value.toInt();
            }

            if(QString::compare("label", childKeys, Qt::CaseInsensitive) == 0)
            {
                flow[state_id]->name = value.toString();
            }

            if(QString::compare("duration", childKeys, Qt::CaseInsensitive) == 0)
            {
                flow[state_id]->duration = value.toDouble();
            }

        }

        state_id++;
        settings->endGroup();
    }

    // assign transitions
    QVectorIterator<Cue *> i[flow];
    while (i.hasNext())
    {
        flow[i]->state->addTransition(this, SIGNAL(timeout()), flow[flow[i]->id]->state);
        flow[i]->state->
    }
    */
}

void GameEngine::start()
{
    // check if error state
    if(engine->isRunning())
        engine->stop();

    // start engine
    engine->start();
    qDebug() << "GameEngine::Start";

}

void GameEngine::pause()
{
    emit(requestPause());
}

void GameEngine::resume()
{
    emit(requestResume());
}

void GameEngine::stop()
{
    qDebug() << "GameEngine::Quit";
    //timer->stop();
    engine->stop();
}

void GameEngine::onStateEngineRuning()
{
    qDebug() << "GameEngine::Running";
}

void GameEngine::onStateEnginePaused()
{
    qDebug() << "GameEngine::Paused";
    timer->stop();
}

