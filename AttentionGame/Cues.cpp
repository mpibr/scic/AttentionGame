#include "Cues.h"

Cues::Cues(QString varname, QState *varstate)
{
    name = varname;
    state = new QState(varstate);
}

CueIdle::CueIdle(QString varname, QState *varstate) : Cues(varname, varstate)
{
}

void CueIdle::parseKeyValuePairs(QString key, QVariant value)
{
    if(QString::compare("transition", key, Qt::CaseInsensitive) == 0)
        this->transition = value.toString();

    if(QString::compare("duration", key, Qt::CaseInsensitive) == 0)
        this->duration = value.toDouble();
}
