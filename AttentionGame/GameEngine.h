#ifndef GAMEENGINE_H
#define GAMEENGINE_H

#include <QObject>
#include <QStateMachine>
#include <QHistoryState>
#include <QTimer>
#include <QSettings>
#include <QDebug>
#include "Cues.h"

class GameEngine : public QObject
{
    Q_OBJECT
public:
    explicit GameEngine(QObject *parent = 0);
    ~GameEngine();
    void parseSettings(QString fileName_config);
    void start();
    void pause();
    void resume();
    void stop();
    void configure(QString config_file);

private:
    QTimer *timer;
    QStateMachine *engine;

    QState *state_engineRunning;
    QState *state_enginePaused;

    QHistoryState *state_engineLastActive;

    QMap<QString, Cues*> flow;

signals:
    void requestPause();
    void requestResume();


private slots:
    void onStateEngineRuning();
    void onStateEnginePaused();


};

#endif // GAMEENGINE_H
