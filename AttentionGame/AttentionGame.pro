#-------------------------------------------------
#
# Project created by QtCreator 2017-04-22T00:19:28
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = AttentionGame
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    GameEngine.cpp \
    Cues.cpp

HEADERS  += mainwindow.h \
    GameEngine.h \
    Cues.h

FORMS    += mainwindow.ui
