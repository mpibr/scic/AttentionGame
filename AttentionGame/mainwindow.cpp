#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    // default button enable
    ui->pushButton_Run->setEnabled(true);
    ui->pushButton_Pause->setEnabled(false);
    ui->pushButton_Stop->setEnabled(false);

    // pushButton_Pause toggle option
    ui->pushButton_Pause->setCheckable(true);
    ui->pushButton_Pause->setChecked(false);

    // set default fileName_config
    fileName_config = "/Users/tushevg/Developer/Projects/Qt/AttentionGame/config/config.ini";

    // construct game engine
    ge = new GameEngine(this);

}

MainWindow::~MainWindow()
{
    delete ui;
}


void MainWindow::on_pushButton_Run_clicked()
{
    qDebug() << "PushButton::Run";

    // buttons order
    ui->pushButton_Run->setEnabled(false);
    ui->pushButton_Pause->setEnabled(true);
    ui->pushButton_Stop->setEnabled(true);

    // create & start state machine
    ge->parseSettings(fileName_config);
    ge->start();


}

void MainWindow::on_pushButton_Stop_clicked()
{
    qDebug() << "PushButton::Stop";

    // default Run button
    ui->pushButton_Run->setEnabled(true);
    ui->pushButton_Pause->setEnabled(false);
    ui->pushButton_Stop->setEnabled(false);

    // default Pause button
    ui->pushButton_Pause->setChecked(false);
    ui->pushButton_Pause->setText("Pause");

    // stop engine
    ge->stop();

}

void MainWindow::on_pushButton_Pause_clicked()
{
    if (ui->pushButton_Pause->isChecked())
    {
        qDebug() << "PushButton::Pause::Checked::True";
        ui->pushButton_Pause->setText("Resume");
        ge->pause();
    }
    else
    {
        qDebug() << "PushButton::Run::Checked::False";
        ui->pushButton_Pause->setText("Pause");
        ge->resume();
    }
}


void MainWindow::on_pushButton_LoadConfig_clicked()
{
    fileName_config = QFileDialog::getOpenFileName(this,
        tr("Open Image"), "/Users/tushevg/Developer/Projects/Qt/AttentionGame/config/", tr("Configuration Files (*.ini)"));

    qDebug() << "File: " << fileName_config;


    // set label text
    QFontMetrics metrix(ui->label->font());
    int width = ui->label->width() - 2;
    QString fileName_clipped = metrix.elidedText(fileName_config, Qt::ElideLeft, width);
    ui->label->setText(fileName_clipped);

}
