#ifndef CUEVISUAL_H
#define CUEVISUAL_H

#include "Cue.h"

class CueVisual : public Cue
{
public:
    CueVisual();
    ~CueVisual();

    virtual void parseKeyValuePair(const QString, const QString, QVariant);

protected:
    QString fileName;
    QString intenristy;
    double angle;


};

#endif // CUEVISUAL_H
