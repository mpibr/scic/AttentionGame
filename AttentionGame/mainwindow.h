#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QPushButton>
#include <QFileDialog>
#include <QDebug>
#include "GameEngine.h"


namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    void on_pushButton_Run_clicked();
    void on_pushButton_Stop_clicked();
    void on_pushButton_LoadConfig_clicked();
    void on_pushButton_Pause_clicked();

private:
    Ui::MainWindow *ui;
    GameEngine *ge;
    QString fileName_config;

};

#endif // MAINWINDOW_H
