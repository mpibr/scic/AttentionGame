#ifndef CUES_H
#define CUES_H

#include <QString>
#include <QVariant>
#include <QState>

class Cues
{
public:
    Cues(QString varname, QState *varstate);
    virtual void parseKeyValuePairs(QString key, QVariant value) = 0;

protected:
    QState *state;
    QString name;
    QString transition;

};

class CueIdle : public Cues
{
public:
    CueIdle(QString varname, QState *varstate);
    void parseKeyValuePairs(QString key, QVariant value);

protected:
    double duration;
};

#endif // CUES_H
